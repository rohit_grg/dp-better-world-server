package au.com.digitalpurpose.betterworld.server.service;

import au.com.digitalpurpose.betterworld.server.model.Goal;
import au.com.digitalpurpose.betterworld.server.model.Idea;
import au.com.digitalpurpose.betterworld.server.repository.IdeaQueries;
import au.com.digitalpurpose.betterworld.server.repository.IdeaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IdeaService {

    private final IdeaRepository ideaRepository;

    @Autowired
    public IdeaService(IdeaRepository ideaRepository) {
        this.ideaRepository = ideaRepository;
    }

    public Iterable<Idea> findIdeas(String searchTerm, Goal goal) {
        return this.ideaRepository.findAll(IdeaQueries.createSearchQuery(searchTerm, goal));
    }

    public Idea getIdea(long id) {
        return this.ideaRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("No idea found for ID " + id));
    }

    public Idea postIdea(Idea newIdea){
        return this.ideaRepository.save(newIdea);
    }



    public Integer increaseIdeaLikes(long id){
        Idea idea = this.ideaRepository.getOne(id);
        idea.increaseLikes();
        this.ideaRepository.save(idea);
        return idea.getLikes();

    }

}
