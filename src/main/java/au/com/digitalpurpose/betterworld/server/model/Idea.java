package au.com.digitalpurpose.betterworld.server.model;

import javax.persistence.*;

@Entity
public class Idea {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Goal goal;

    private String title;
    private String description;
    private String thumbnailUrl;
    private Integer likes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Integer getLikes(){
        return likes;
    }

    public void setLikes(Integer numberOfLikes){
        this.likes = numberOfLikes;
    }

    public void increaseLikes(){
        setLikes(this.getLikes() + 1);
    }

    @Override
    public String toString() {
        return "Idea{" +
                "id=" + id +
                ", goal=" + goal +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                '}';
    }
}
